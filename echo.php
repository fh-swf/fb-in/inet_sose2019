<!doctype html>
<head>
    <style>
        * {
            font-family: "IBM Plex Sans";
        }
        thead * {
            background-color: darkgrey; 
            color: white;
        }
        td, th {
            text-align: left;
            padding: 5px;
        }
        tr:nth-child(2n) {
            background-color: #d0d0d0;
        }
    </style>
</head>

<html>

<body>
    <h1>Request Data</h1>

    <table>
        <thead>
            <tr><th>key</th><th>value</th><th>sanitized value</th></tr>
        </thead>
        <tbody>
          <?php
             foreach ($_REQUEST as $name => $value) {
                echo "<tr><td>$name</td> <td>$value</td>";
                $value = filter_input(INPUT_POST, $name, FILTER_SANITIZE_SPECIAL_CHARS);
                echo "<td>$value</td></tr>\n";
             }
          ?>
    </table>

    <h2>Header</h2>
    <table>
        <thead>
            <tr><th>key</th><th>value</th></tr>
        </thead>
        <tbody>
           <?php
              foreach (getallheaders() as $name => $value) {
                 echo "<tr><td>$name</td> <td>$value</td></tr>\n";
              }
        ?>
        </tbody>
    </table>
</body>

</html>