/**
 * Beispielapplikation "Zentraler Pizzaservice" aus der Vorlesung Internettechnologien, Sommersemester 2019
 * Diese Datei implementiert den Webserver mithilfe von Express.js
 *
 * @copyright 2019 Christian Gawron <gawron.christian@fh-swf.de>
 * @license MIT
 */
const express = require('express');
const cors = require('cors');
const api = require('./api.js');
const app = express();

// define "middlewares"
app.use(express.static('static'));
app.use(cors());
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({
    extended: true
})); // for parsing application/x-www-form-urlencoded

// API definition: register callbacks for routes & methods
app.get('/api/order', api.getOrders);
app.post('/api/order', api.createOrder);

app.get('/api/order/:id', api.getOrder);
app.put('/api/order/:id', api.saveOrder);
app.delete('/api/order/:id', api.deleteOrder);

api.init()
    .then(() => {
        app.listen(8080);
    })
    .catch(error => {
        console.log("initialization error: %o", error);
    });