<?php

// "normale" Felder mit textuellen Werten
$keys = [ 
    'customer',
    'phone',
    'email',
    'location',
    'choice',
    'delivery_time',
    'delivery_date',
    'instructions'
];

// Ankreuzfelder mit boolschen Werten. Werden in der Datenbank auf 0/1 abgebildet.
$bool_keys = [
    'mozzarella',
    'zwiebeln',
    'champignons',
    'salami'
];

// Datenbank
$host = '127.0.0.1';
$db   = 'pizza';
$user = 'root';
$pass = '';
$charset = 'utf8mb4';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
];

$pdo = new PDO($dsn, $user, $pass, $options);

/**
 * Speichere eine Order.
 * Die Information, ob ein Update bzw. ein Insert erfolgen soll, wird über den Parameter $update mitgegeben.
 * @todo  Error-Handling sowie ggf. automatische Erkennung, ob INSERT/UPDATE notwendig ist.
 * @param $id ID der Order
 * @param $update Wenn false, lege einen neuen Datensatz an.
 */
function store_order($id, $update = false) {
    if ($update) {
        update_order($id);
    } else {
        insert_order($id);
    }
}

function insert_order($id) {
    global $pdo;
    $sql = 'INSERT INTO orders (id, customer, phone, email, location, choice, size, mozzarella, zwiebeln, 
                                champignons, salami, delivery_time, delivery_date, instructions) 
                   VALUES (:id, :customer, :phone, :email, :location, :choice, :size, :mozzarella, :zwiebeln, 
                           :champignons, :salami, :delivery_time, :delivery_date, :instructions) ';
                
    $insert = $pdo->prepare($sql);
    $params = get_params($id);
    $insert->execute($params);
}

function update_order($id) {
    global $pdo;
    $sql = 'UPDATE orders SET customer = :customer, phone = :phone, email = :email, location = :location, choice = :choice, 
                              size = :size, mozzarella = :mozzarella, zwiebeln = :zwiebeln, champignons = :champignons, salami = :salami, 
                              delivery_time = :delivery_time, delivery_date = :delivery_date, instructions = :instructions
                   WHERE id = :id';
                
    $update = $pdo->prepare($sql);
    $params = get_params($id);
    $update->execute($params);
}

/**
 * Liefert ein Array mit allen benötigten Daten für INSERT bzw. UPDATE.
 * Die Daten werden aus $_POST gelesen und - soweit nötig - passend konvertiert.
 * @param $id ID des Datensatzes (ist nicht im Formular enthalten sondern wird über den URI übergeben)
 * @return Array mit den Daten
 */
function get_params($id) {
    global $keys, $bool_keys;

    $params[':id'] = $id;
    $params += get_values($keys);
    $params += get_bools($bool_keys);
    $params[':size'] = get_size();

    return $params;
}

function get_values($keys) {
    foreach($keys as $key) {
        $value = NULL;
        if (isset($_POST[$key])) {
            $value = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }
        $params[":$key"] = $value;
    }
    return $params;
}

function get_bools($keys) {
    foreach($keys as $key) {
        $value = 0;
        if (isset($_POST[$key])) {
            $value = 1;
        }
        $params[":$key"] = $value;
    }
    return $params;
}

function get_size() {
    switch ($_POST['size']) {
        case 'medium': return 1; break;
        case 'large': return 2; break;
        default: return 0; break;
    }
}

function get_order($id) {
    global $pdo, $keys;
    $sql = 'SELECT * FROM orders WHERE id = :id';
    $select = $pdo->prepare($sql);
    $select->execute([ ':id' => $id ]);
    $result = $select->fetch();
    return $result;
}

function delete_order($id) {
    global $pdo, $keys;
    $sql = 'DELETE FROM orders WHERE id = :id';
    $delete = $pdo->prepare($sql);
    $delete->execute([ ':id' => $id ]);
}

function print_order($data) {
    // Variablen für die Lokation 
    $is_hagen = $data['location'] == 'hagen' ? 'selected="true"' : '';
    $is_iserlohn = $data['location'] == 'iserlohn' ? 'selected="true"' : '';
    $is_meschede = $data['location'] == 'meschede' ? 'selected="true"' : '';
    $is_soest = $data['location'] == 'soest' ? 'selected="true"' : '';
    // Variablen für die Größe
    $is_small = $data['size'] == 0 ? 'checked="true"' : '';
    $is_medium = $data['size'] == 1 ? 'checked="true"' : '';
    $is_large = $data['size'] == 2 ? 'checked="true"' : '';
    // Variablen für die Extras
    $is_mozzarella = $data['mozzarella'] > 0 ? 'checked="true"' : '';
    $is_zwiebeln = $data['zwiebeln'] > 0 ? 'checked="true"' : '';
    $is_champignons = $data['champignons'] > 0 ? 'checked="true"' : '';
    $is_salami = $data['salami'] > 0 ? 'checked="true"' : '';

    $url = ORDER_URI . "/$data[id]";

    $html = <<<EOD
    <form method="post" action="$url">
        <fieldset>
            <legend> Kundendaten </legend>
            <p><label for="customer">Name:</label><input id="customer" name="customer" required="true" value="{$data['customer']}"/></p>
            <p><label for="phone">Telefon:</label><input id="phone" type="tel" name="phone" value="{$data['phone']}" /></p>
            <p><label for="email">Email:</label><input id="email" type="email" name="email" value="{$data['email']}"/></p>
            <p>
                <label for="location">Standort:</label>
                <select id="location" name="location">
                    <option value="hagen" $is_hagen>Hagen</option>
                    <option value="iserlohn" $is_iserlohn>Iserlohn</option>
                    <option value="meschede" $is_meschede>Meschede</option>
                    <option value="soest" $is_soest>Soest</option>
                </select>
            </p>
        </fieldset>

        <label for="choice">Gewünschte Pizza:</label>
        <input list="pizza-flavors" id="choice" name="choice" required="true" value="{$data['choice']}"/>

        <fieldset>
            <legend> Größe </legend>
            <div><label> <input type="radio" name="size" value="small" $is_small > Klein </label></div>
            <div><label> <input type="radio" name="size" value="medium" $is_medium > Mittel </label></div>
            <div><label> <input type="radio" name="size" value="large" $is_large > Groß </label></div>
        </fieldset>

        <fieldset class="block">
            <legend> Extras </legend>
            <label> <input type="checkbox" name="mozzarella" $is_mozzarella /> Mozzarella </label>
            <label> <input type="checkbox" name="zwiebeln" $is_zwiebeln /> Zwiebeln </label>
            <label> <input type="checkbox" name="champignons" $is_champignons /> Champignons </label>
            <label> <input type="checkbox" name="salami" $is_salami /> Salami </label>
        </fieldset>

        <p>
            <label>Lieferung: <input type="time" name="delivery_time" min="11:00" max="21:00" step="900" value="{$data['delivery_time']}" required="true"/>
                <input type="date" name="delivery_date" value="{$data['delivery_date']}"/></label>
        </p>

        <p><label for="instructions">Sonderwünsche:</label>
            <br /><textarea id="instructions" name="instructions" rows="3">{$data['instructions']}</textarea></p>

        <input type="submit" name="delete" value="Bestellung stornieren" />
        <input type="submit" name="submit" value="Bestellung ändern" />
      
        <datalist id="pizza-flavors">
            <option value="Margherita">
            <option value="Funghi">
            <option value="Salami">
            <option value="Prosciutto">
            <option value="Tonno">
        </datalist>
    </form>
EOD;
    echo($html);
}

function print_not_found() {
    $html = <<<EOD
    <h1>Oops, da ist etwas schief gegangen!</h1>
    <p>Es tut mir wirklich <strong>sehr leid</strong> &mdash; die von Ihnen genannte Bestellung kann ich bei uns nicht finden!</p>
    <p>Bitte geben Sie <a href="/pizza/order.html">hier</a> eine neue Bestellung auf.</p>
EOD;
    echo($html);
}

?>