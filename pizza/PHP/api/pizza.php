<?php 
    define('ORDER_URI', '/pizza/api/order');
    define('ORDER_REGEX', '/' . addcslashes(ORDER_URI . '/(.+)', '/') . '/');
    $debug = FALSE;

    include_once('persistence.php');

$delete = $update = $insert = FALSE;

/*
 * Was ist zu tun? Zunächst entscheiden wir anhand der Request-Methode und der URI, 
 * welche Aktion wir durchführen (CREATE, READ, UPDATE, DELETE) und "besorgen" 
 * notwendige Daten. Die Anzeige der Ergebnisse erfolgt weiter unten.
 */
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // POST request - Anlegen einer neuen Bestellung, Änderung oder Stornierung 

    $delete = isset($_POST['delete']);
    if (preg_match(ORDER_REGEX, $_SERVER['REQUEST_URI'], $matches)) {
        // Bestelländerung - ID ist Bestandteil der URI
        $order = $matches[1];
        $update = TRUE;
    } else {
        // neue Bestellung - erzeuge ID
        $order = uniqid();
        $insert = TRUE;
    }
    $order_uri = ORDER_URI . "/$order";
    $choice = filter_input(INPUT_POST, 'choice', FILTER_SANITIZE_SPECIAL_CHARS);
    $customer = filter_input(INPUT_POST, 'customer', FILTER_SANITIZE_SPECIAL_CHARS);
    $delivery = filter_input(INPUT_POST, 'delivery_time', FILTER_SANITIZE_SPECIAL_CHARS);

    if ($delete) {
        delete_order($order);
    } else {
        // Schreibe Order in Datenbank
        store_order($order, $update);
    }
} else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // GET request - Abfrage einer existierenden Bestellung
    preg_match(ORDER_REGEX, $_SERVER['REQUEST_URI'], $matches);
    $order = $matches[1];
}
?>

<?php // Darstellung der Ergebnisse ?>
<?php if ($_SERVER['HTTP_ACCEPT'] == 'application/json'):
    // JSON Modus
    if ($delete) {
        //TODO
    } else if ($update || $insert) {
        //TODO
    } else {
        header('Content-Type: application/json');
        $data = get_order($order);
        print(json_encode($data, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK));
    }
else: ?>
<!doctype html>

<html>
<head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/png" href="/pizza/favicon.png" sizes="192x192">
    <link rel="stylesheet" type="text/css" href="/pizza/main.css" />
</head>
<body>

<?php include_once('debug.php'); ?>

<?php if ($delete): ?>
    <h1>Schade, dass Sie Ihre Bestellung storniert haben!</h1>
    <p>Ihre Bestellung einer <em>Pizza <?php echo ($choice); ?></em> haben wir wie gewünscht storniert.</p>
    <p>Wir hoffen, dass Sie uns bald wieder besuchen und eine neue <a href="/pizza/order.html">Bestellung</a> aufgeben.</p>
    <p>Bis dahin wünschen wir Ihnen einen schönen Tag, <?php echo ($customer); ?>!
    </p>
<?php elseif ($update || $insert): ?>
    <h1>Vielen Dank!</h1>
    <p>Vielen Dank für Ihre Bestellung einer <em>Pizza <?php echo ($choice); ?></em>, die wir Ihnen wie gewünscht um <em><?php echo ($delivery); ?></em> 
    ausliefern werden.</p>
    <p>Bis dahin wünschen wir Ihnen einen schönen Tag, <?php echo ($customer); ?>!</p>
    <p>Ihre Bestellung können Sie jederzeit <a href="<?php echo ($order_uri); ?>">hier</a> einsehen und bis 45 Minuten vor dem Liefertermin
    ändern bzw. stornieren.</p>
<?php else: ?>
    <?php
       // Lese Orderdaten aus der Datenbank und gebe sie aus 
       $data = get_order($order);

       // Prüfe, welche MIME-Type der Client akzeptiert
       if ($data) {
            echo("<h1>Status Ihrer Bestellung</h1>");
            echo("<p>Guten Tag, $data[customer]. Hier die Details Ihrer Bestellung:</p>");
            print_order($data);
        } else {
            // Error-Handling, wenn die Bestellung nicht gefunden wurde.
            print_not_found();
        } 
    ?>
<?php endif; ?>

</body>
</html>
<?php endif; ?>