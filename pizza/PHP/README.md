# Demo: Pizza-Service
In dieser Demo aus der Vorlesung geht es um das Bestellformular des 
"Zentralen Pizza Service der Fachhochschule Südwestfalen" (der leider 
bisher nur in dieser Demo existiert).

Die Anwendung erlaubt das Aufgeben, Ändern und Stornieren einer Bestellung.

## Installation
Die Anwendung verwendet PHP und läuft unter XAMPP. Auf einer 
Standardinstallation von XAMPP sind die folgenden Schritte notwendig:

1. Anlegen einer Datenbank `pizza`. Dies erledigt man am einfachsten über phpMyAdmin.
2. Anlegen der Tabelle `orders`. Das nötige DDL-Script befindet sich im Ordner
   ddl.
3. Einrichtung des URI-Mappings. Dazu müssen in der `httpd.conf` (unter Windows bei XAMPP standardmäßig unter `C:\xampp\apache\conf\httpd.conf`) 
   folgende Statements ergänzt werden:
   ```
   <LocationMatch "/pizza/(?<PATH>.*)">
      Alias "C:/xampp/htdocs/inet_sose2019/pizza/%{env:MATCH_PATH}"
   </LocationMatch>

   <LocationMatch "/pizza/api/.*">
  	  ScriptAlias "C:/xampp/htdocs/inet_sose2019/pizza/api/pizza.php"
   </LocationMatch>
   ```
   
## Verwendete URIs
Das (statische) [Bestellformular](http://localhost/pizza/order.html) ist unter `http://localhost/pizza/order.html` erreichbar. 
Einzelne Order ruft man über `http://localhost/pizza/api/order/{order-id}` ab.