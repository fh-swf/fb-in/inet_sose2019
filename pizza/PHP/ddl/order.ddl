-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `orders`
--

CREATE TABLE `orders` (
  `id` varchar(32) COLLATE utf8_bin NOT NULL,
  `customer` varchar(64) COLLATE utf8_bin NOT NULL,
  `phone` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `location` varchar(64) COLLATE utf8_bin NOT NULL,
  `choice` varchar(64) COLLATE utf8_bin NOT NULL,
  `size` int(11) NOT NULL,
  `mozzarella` tinyint(1) DEFAULT NULL,
  `zwiebeln` tinyint(1) DEFAULT NULL,
  `champignons` tinyint(1) DEFAULT NULL,
  `salami` tinyint(1) DEFAULT NULL,
  `delivery_time` varchar(8) COLLATE utf8_bin NOT NULL,
  `delivery_date` date NOT NULL,
  `instructions` varchar(256) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);